#!/usr/bin/env python
import MySQLdb
import pprint
import sys
import datetime
import optparse
import json
import time

import codereviewdb

class TagReportIndexException(Exception):
    pass

class TagReportIndex(object):
    '''
    Keeps track of revisions in a way that snapshots of state are easy
    to generate.
    '''
    def __init__(self, crdb, targetver):
        self._targetver = targetver
        self._build_rev_index(crdb)
        self._build_tag_index(crdb)
        self._build_workrevs(crdb)
        self.report = self._build_report(crdb)

    def _build_rev_index(self, crdb):
        self._revs = {}
        for revrow in crdb.rev_rows:
            self._revs[revrow['cr_id']]=revrow

    def _build_tag_index(self, crdb):
        self.tagindex = {}
        self.revnums = []
        for revtag in crdb.rev_tags:
            rev = revtag['cr_id']
            tag = revtag['ct_tag']
            if tag not in self.tagindex:
                self.tagindex[tag] = []
            self.tagindex[tag].append(rev)
            if rev not in self._revs:
                self._revs[rev] = {}
            if 'tags' not in self._revs[rev]:
                self._revs[rev]['tags'] = []
            self._revs[rev]['tags'].append(tag)

    def _is_target_ver(self, rev, revstatus):
        if self._targetver == 'trunk':
            return ((revstatus == 'new' or revstatus == 'fixme') and
                    (not self.rev_has_tag(rev, 'nodeploy')))
        elif self._targetver == '1.19':
            return ((rev in self.tagindex['1.19'] or rev in self.tagindex['1.19wmf1'] or rev < 110996) and
                    (revstatus == 'new' or revstatus == 'fixme') and
                    (not self.rev_has_tag(rev, 'nodeploy') and
                    not self.rev_has_tag(rev, 'reverted1.19') and
                    not self.rev_has_tag(rev, '1.19ok')))
        elif self._targetver == '1.18':
            return ((rev in self.tagindex['1.18'] or rev < 92475) and
                    (revstatus == 'new' or revstatus == 'fixme') and
                    (not self.rev_has_tag(rev, 'nodeploy') and
                    not self.rev_has_tag(rev, 'reverted1.18') and
                    not self.rev_has_tag(rev, '1.18ok')))
        else:
            raise CodeReviewStatsException

    def rev_has_tag(self, rev, tag):
        if 'tags' in self._revs[rev]:
            return tag in self._revs[rev]['tags']
        else:
            return False

    def _build_workrevs(self, crdb):
        def in_target_ver_needwork(rev):
            try:
                revstatus = self._revs[rev]['cr_status']
            except KeyError:
                revstatus = 'unknown'
            return self._is_target_ver(rev, revstatus)
        def in_fixme(rev):
            return (self._revs[rev]['cr_status'] == 'fixme')
        def in_new(rev):
            return (self._revs[rev]['cr_status'] == 'new')
        revnums = sorted(self._revs.keys())
        self.workrevnums = filter(in_target_ver_needwork, revnums)
        self.fixme_count = len(filter(in_fixme, self.workrevnums))
        self.new_count = len(filter(in_new, self.workrevnums))

    def _build_new_report(self, crdb):
        retval = {'untagged':[]}
        for rev in self.workrevnums:
            untagged = True
            revstatus = self._revs[rev]['cr_status']
            if revstatus != 'new':
                continue
            if 'tags' in self._revs[rev]:
                for tag in self._revs[rev]['tags']:
                    if tag == '1.18':
                        continue
                    untagged = False
                    if tag not in retval:
                        retval[tag] = []
                    retval[tag].append(rev)
            if untagged:
                retval['untagged'].append(rev)
        return retval

    def _author_report_for_state(self, crdb, state):
        retval = {}
        for rev in self.workrevnums:
            if self.rev_has_tag(rev, 'nodeploy') or self._revs[rev]['cr_status'] != state:
                continue
            author = self._revs[rev]['cr_author']
            if author not in retval:
                retval[author] = []
            retval[author].append(rev)
        return retval

    def _build_report(self, crdb):
        retval = {}
        retval['totalrevs'] = len(self.workrevnums)
        retval['totalfixme'] = self.fixme_count
        retval['totalnew'] = self.new_count
        retval['new'] = self._build_new_report(crdb)
        retval['byauthor'] = {}
        retval['byauthor']['new'] = self._author_report_for_state(crdb, 'new')
        retval['byauthor']['fixme'] = self._author_report_for_state(crdb, 'fixme')
        return retval


class OutputSink(object):
    '''
    Object which keeps track of what/where to output stuff
    '''

    def __init__(self, format='json'):
        self.format = format

    def output(self, tagindex):
        if self.format == 'json':
            # print out ready-to-use javascript
            print json.dumps(tagindex, sort_keys=True, indent=4)
        elif self.format == 'wiki':
            print self.wikimarkup(tagindex)

    def wikimarkup(self, tagindex):
        retval = "* Total revisions: "
        retval += str(tagindex['totalrevs'])
        retval += "; new: %i" % tagindex['totalnew']
        retval += "; fixme: %i\n" % tagindex['totalfixme']
        retval += "== New revisions by tag ==\n"
        retval += "[//www.mediawiki.org/w/index.php?path=%2Ftrunk&title=Special%3ACode%2FMediaWiki%2Fstatus%2Fnew "
        retval += "Total new revisions: %i]\n" % tagindex['totalnew']
        retval += '{| class="wikitable sortable"'
        retval += "\n"
        retval += "! Tag !! Revisions\n"
        for tag in sorted(tagindex['new'].keys()):
            retval += "|-\n"
            retval += "| "
            if tag == "untagged":
                retval += tag
            else:
                retval += "[//www.mediawiki.org/w/index.php?path=%2Ftrunk&status=new&title=Special:Code/MediaWiki/tag/" + tag + " " + tag + "]"
            revstrs = []
            count = 0
            for rev in tagindex['new'][tag]:
                revstr = "[[Special:Code/MediaWiki/%d|%d]]" % (rev, rev)
                revstrs.append(revstr)
                count += 1
            retval += " ||data-sort-value=%d|" % count
            retval += ", ".join(revstrs)
            retval += "\n"
        retval += "|}\n\n"
        retval += "== New revisions by author ==\n"
        retval += self.wikimarkup_for_state(tagindex['byauthor']['new'], 'new')
        retval += "== Fixmes ==\n"
        retval += "[//www.mediawiki.org/w/index.php?path=%2Ftrunk&title=Special%3ACode%2FMediaWiki%2Fstatus%2Ffixme "
        retval += "Total revisions marked 'fixme': %i]\n" % tagindex['totalfixme']
        retval += self.wikimarkup_for_state(tagindex['byauthor']['fixme'], 'fixme')
        return retval

    def wikimarkup_for_state(self, reportdata, state):
        retval = ""
        retval += '{| class="wikitable sortable"'
        retval += "\n"
        retval += "! Author !! Revisions\n"
        for author in sorted(reportdata.keys()):
            retval += "|-\n"
            retval += "| [//www.mediawiki.org/wiki/Special:Code/MediaWiki/status/"
            retval += state
            retval += "?author="
            retval += author + " " + author + "]"
            revstrs = []
            count = 0
            for rev in reportdata[author]:
                revstr = "[[Special:Code/MediaWiki/%d|%d]]" % (rev, rev)
                revstrs.append(revstr)
                count += 1
            retval += " ||data-sort-value=%d|" % count
            retval += ", ".join(revstrs)
            retval += "\n"
        retval += "|}\n"
        return retval

def _get_options():
    usage = "usage: %prog [options]"
    parser = optparse.OptionParser(usage)
    parser.add_option("-c", "--cache", dest="cache", type="str")
    parser.add_option("-d", "--dumpcache", dest="dumpcache", type="str",
        help="Write the cache out as a CSV file for debug purposes")
    parser.add_option("-w", "--writecache", dest="writecache", type="str")
    parser.add_option("-f", "--format", dest="format", type="str", default="json")
    parser.add_option("-q", "--querytag", dest="querytag", type="str", default="trunkall")
    parser.add_option("-t", "--targetver", dest="targetver", type="str", default="trunk")
    return parser.parse_args()

def main(options, args):
    # Perform db queries (or load from cache), and stuff results into a CodeReviewDB obj
    crdb = codereviewdb.CodeReviewDB(readcache=options.cache, writecache=options.writecache, querytag=options.querytag)

    # Process the results, building the data structure for the report
    trindex = TagReportIndex(crdb, options.targetver)
    crdb.close()

    # Output the results in the selected format
    sink = OutputSink(format=options.format)
    sink.output(trindex.report)

if __name__ == "__main__":
    (options, args) = _get_options()
    main(options, args)

