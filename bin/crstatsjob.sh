#!/bin/bash
#$ -o /home/robla/tmp/crstatsjob-stdout.log 
#$ -e /home/robla/tmp/crstatsjob-error.log 
set -e
CRSTATSDIR=/home/robla/public_html/crstats
for CRQUERYTAG in "1.18phase3" "1.18all" "trunkphase3" "trunkall"; do
	CRJSDATEDDATA=$CRSTATSDIR/data/${CRQUERYTAG}/crstatsdata-`date +%Y-%m-%d`.js
	CRJSDATA=$CRSTATSDIR/data/${CRQUERYTAG}/crstatsdata.js
	/home/robla/bin/codereviewstats.py -q "${CRQUERYTAG}" > $CRJSDATEDDATA
	rm -f $CRJSDATA
	ln -s $CRJSDATEDDATA $CRJSDATA
done

