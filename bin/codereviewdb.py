#!/usr/bin/env python
import MySQLdb
import pprint
import sys
import datetime
import optparse
import json
import time

def get_query_set(tag):
    return CodeReviewQuerySet

class CodeReviewStatsException(Exception):
    pass

class CodeReviewQuerySet(object):
    querysets = { '1.17phase3': {'firstrev':47450, 'lastrev':77973, 'pathspec':'/trunk/phase3'},
                  '1.17all': {'firstrev':47450, 'lastrev':77973, 'pathspec':'/trunk'},
                  '1.18phase3': {'firstrev':47450, 'lastrev':92475, 'pathspec':'/trunk/phase3'},
                  '1.18all': {'firstrev':47450, 'lastrev':92475, 'pathspec':'/trunk'},
                  '1.19phase3': {'firstrev':47450, 'lastrev':110996, 'pathspec':'/trunk/phase3'},
                  '1.19all': {'firstrev':47450, 'lastrev':110996, 'pathspec':'/trunk'},
                  'trunkphase3': {'firstrev':47450, 'lastrev':None, 'pathspec':'/trunk/phase3'},
                  'trunkall': {'firstrev':47450, 'lastrev':None, 'pathspec':'/trunk'} }
    def __init__(self, tag):
        self.rev_query = self.make_rev_query(firstrev=self.querysets[tag]['firstrev'],
                                             lastrev=self.querysets[tag]['lastrev'],
                                             pathspec=self.querysets[tag]['pathspec'])
        self.prop_change_query = self.make_prop_change_query(firstrev=self.querysets[tag]['firstrev'],
                                                             lastrev=self.querysets[tag]['lastrev'],
                                                             pathspec=self.querysets[tag]['pathspec'])
        self.tag_query = self.make_tag_query(firstrev=self.querysets[tag]['firstrev'],
                                             lastrev=self.querysets[tag]['lastrev'],
                                             pathspec=self.querysets[tag]['pathspec'])

    def make_rev_query(self, firstrev=None, lastrev=None, pathspec=None):
        clauses = ['cr_repo_id=1']
        if firstrev is not None:
            clauses.append('cr_id>='+str(firstrev))
        if lastrev is not None:
            clauses.append('cr_id<='+str(lastrev))
        if pathspec is not None:
            clauses.append("cr_path LIKE '" + pathspec + "%'")
        return ("SELECT " +
            "cr_id, cr_path, cr_status, cr_timestamp, cr_author " +
            "FROM code_rev WHERE " +
            ' AND '.join(clauses) + " " +
            "ORDER BY cr_timestamp")

    def make_prop_change_query(self, firstrev=None, lastrev=None, pathspec=None):
        clauses = ['cr_repo_id=1', "cpc_attrib='status'"]
        if firstrev is not None:
            clauses.append('cpc_rev_id>='+str(firstrev))
        if lastrev is not None:
            clauses.append('cpc_rev_id<='+str(lastrev))
        if pathspec is not None:
            clauses.append("cr_path LIKE '" + pathspec + "%'")

        return ("SELECT " +
            "cpc_rev_id, cpc_added, cpc_removed, cpc_timestamp, cpc_user_text " +
            "FROM code_prop_changes LEFT JOIN code_rev ON cpc_rev_id = cr_id " +
            "WHERE  " +
            ' AND '.join(clauses) + " " +
            "ORDER BY cpc_timestamp")

    def make_tag_query(self, firstrev=None, lastrev=None, pathspec=None):
        clauses = ['cr_repo_id=1']
        if firstrev is not None:
            clauses.append('cr_id>='+str(firstrev))
        if lastrev is not None:
            clauses.append('cr_id<='+str(lastrev))
        if pathspec is not None:
            clauses.append("cr_path LIKE '" + pathspec + "%'")

        return ("SELECT ct_tag, cr_id " +
            "FROM code_tags LEFT JOIN code_rev ON ct_rev_id = cr_id " +
            "WHERE  " +
            ' AND '.join(clauses) + " " +
            "ORDER BY cr_id")

class CodeReviewDB(object):
    '''
    Object to directly access the toolserver copy of the code review DB,
    with generator to get the property changes for each revision.
    '''
    def __init__(self, readcache=None, writecache=None, dumpcache=None, querytag=None):
        self.db = None
        if(readcache is None or writecache is not None):
            try:
                self.db = MySQLdb.connect(host="sql-s3-rr",
                    db="mediawikiwiki_p",
                    read_default_file="~/.my.cnf")
            except MySQLdb.Error, err:
                print "MySQLdb error %d: %s" % (err.args[0], err.args[1])
                sys.exit(1)
            queryset = CodeReviewQuerySet(querytag)
            self.rev_rows = self.load_query(queryset.rev_query)
            self.prop_rows = self.load_query(queryset.prop_change_query)
            self.rev_tags = self.load_query(queryset.tag_query)
        if(writecache is not None):
            dumpobj = {'rev_rows':self.rev_rows, 'prop_rows':self.prop_rows, 'rev_tags':self.rev_tags}
            fp = open(writecache, 'w')
            json.dump(dumpobj, fp)
            fp.close()
        if(readcache is not None):
            fp = open(readcache, 'r')
            dumpobj = json.load(fp)
            fp.close
            self.rev_rows = dumpobj['rev_rows']
            self.prop_rows = dumpobj['prop_rows']
            self.rev_tags = dumpobj['rev_tags']
        if(dumpcache is not None):
            self.dump_csv_cache(dumpcache)

    def load_query(self, query):
        '''Pull all revisions from the DB'''
        cursor = MySQLdb.cursors.DictCursor(self.db)
        cursor.execute(query)
        result = cursor.fetchall()
        cursor.close()
        return result

    def prop_change_row(self, rownum):
        row = self.prop_rows[rownum]
        return {'type':'statuschange',
                'rev':row['cpc_rev_id'],
                'old':row['cpc_removed'],
                'new':row['cpc_added'],
                'timestamp':row['cpc_timestamp']}

    def rev_change_row(self, rownum):
        row = self.rev_rows[rownum]
        return {'type':'newrev',
                'path':row['cr_path'],
                'laststatus':row['cr_status'],
                'timestamp':row['cr_timestamp'],
                'rev':row['cr_id']}

    def newrevs(self):
        '''
	Return all new revs
        '''
        rev_row_num = 0
        while(rev_row_num < len(self.rev_rows)):
            yield self.rev_change_row(rev_row_num)
            rev_row_num += 1

    def reverse_changes(self):
        '''
        Generator that zips up the array of new revisions (which are
        technically status changes) with the array of status changes
        (which are literally status changes) in reverse timestamp order.
        '''
        rev_row_num = len(self.rev_rows)-1
        prop_row_num = len(self.prop_rows)-1
        while(rev_row_num > -1 or prop_row_num > -1):
            if rev_row_num > -1:
                rev_timestamp = self.rev_rows[rev_row_num]['cr_timestamp']
            if prop_row_num > -1:
                prop_timestamp = self.prop_rows[prop_row_num]['cpc_timestamp']
            if prop_timestamp >= rev_timestamp and prop_row_num > -1:
                yield self.prop_change_row(prop_row_num)
                prop_row_num -= 1
            elif rev_row_num > -1:
                yield self.rev_change_row(rev_row_num)
                rev_row_num -= 1
            elif prop_row_num > -1:
                yield self.prop_change_row(prop_row_num)
                prop_row_num -= 1
            else:
                # we shouldn't ever get here
                raise CodeReviewStatsException

    def dump_csv_cache(self, csvfile):
        import csv
        fp = open(csvfile, 'w')
        csv_writer = csv.writer(fp)
        csv_writer.writerow(["id","timestamp","author","cr_path","cr_status","cpc_removed","cpc_added"])
        for row in self.rev_rows:
            outrow = []
            outrow.append(row['cr_id'])
            outrow.append(row['cr_timestamp'])
            outrow.append(row['cr_author'])
            outrow.append(row['cr_path'])
            outrow.append(row['cr_status'])
            outrow.append('')
            outrow.append('')
            csv_writer.writerow(outrow)
        for row in self.prop_rows:
            outrow = []
            outrow.append(row['cpc_rev_id'])
            outrow.append(row['cpc_timestamp'])
            outrow.append(row['cpc_user_text'])
            outrow.append('')
            outrow.append('')
            outrow.append(row['cpc_removed'])
            outrow.append(row['cpc_added'])
            csv_writer.writerow(outrow)
        fp.close()

    def close(self):
        if self.db is not None:
            self.db.close()


