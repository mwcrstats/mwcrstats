#!/usr/bin/env python
import MySQLdb
import pprint
import sys
import datetime
import dateutil
import dateutil.parser
import optparse
import json
import time

import codereviewdb


class RevisionStateTracker(object):
    '''
    Keeps track of revisions in a way that snapshots of state are easy 
    to generate. 
    '''
    def __init__(self, crdb, begindate):
        self.revstates = {}
        self.states = set()
        self.revisions = {}
        self.snapshots = {}
        self.begindate = dateutil.parser.parse(begindate)
        self.build_tracker(crdb)

    def add_rev(self, id, path, laststatus, timestamp):
        self.revisions[id]={}
        self.revisions[id]['path']=path
        self.revisions[id]['status']=laststatus
        self.revisions[id]['laststatus']=laststatus
        self.revisions[id]['timestamp']=timestamp
        self.states.add(laststatus)

    def remove_rev(self, id):
        del self.revisions[id]

    def change_rev_state(self, revision, fromstate, tostate):
        try:
            oldstate = self.revisions[revision]['status']
        except KeyError:
            oldstate = fromstate

        #TODO - check that fromstate == oldstate

        try:
            self.revisions[revision]['status'] = tostate
        except KeyError:
            self.revisions[revision] = {}
            self.revisions[revision]['status'] = tostate
            
        if(tostate != ''):
            self.states.add(tostate)

    def get_tally(self):
        tally = {}
        for state in self.states:
            tally[state]=0

        for rev in self.revisions.keys():
            tally[self.revisions[rev]['status']] += 1
        return tally

    def dump_stats(self):
        tally = self.get_tally()
        for state in self.states:
            print state + ": " + str(tally[state]),
        print
    
    def take_snapshot(self, datestr):
        self.snapshots[datestr] = self.get_tally()
        
    def get_snapshots_by_status(self):
        retval = {}
        for state in self.states:
            retval[state] = []
        for datestr in sorted(self.snapshots.keys()):
            for label in self.snapshots[datestr].keys():
                retval[label].append([datestr, self.snapshots[datestr][label]])
        return retval

    def build_tracker(self, crdb):
        def datestr(date):
            return date.strftime("%Y%m%d000000")

        # first iterate through the changes, and get the newrevs
        for change in crdb.newrevs():
	    self.add_rev(change['rev'], change['path'], 
                change['laststatus'], change['timestamp'])

        # build the snapshots in reverse order.  We need to build in reverse to
        # get a proper "firststatus", since it's not guaranteed to be "new".
        # Since we know the end status, we derive the first status by walking
        # the status changes from the end.
        snapdate = datetime.date.today()
        newnew = 0
        new2not = 0
        new2ok = 0
        new2deferred = 0
        lastsnap = int(time.mktime(datetime.datetime.now().timetuple()))*1000
        self.take_snapshot(lastsnap)
        for change in crdb.reverse_changes():
            if change['timestamp'] < datestr(snapdate):
                timestamp=int(time.mktime(snapdate.timetuple()))*1000
                self.take_snapshot(timestamp)
                if snapdate.weekday() == 0:
                    self.snapshots[lastsnap]['newly new']=newnew
                    self.snapshots[lastsnap]['newly reviewed']=new2not
                    self.snapshots[lastsnap]['newly ok']=new2ok
                    self.snapshots[lastsnap]['newly deferred']=new2deferred
                    lastsnap=timestamp
                    newnew=0
                    new2not=0
                    new2ok=0
                    new2deferred=0
                snapdate -= datetime.timedelta(days=1)
            revid = change['rev']
            if change['type']=='newrev':
                self.remove_rev(change['rev'])
                newnew+=1
            elif change['type']=='statuschange':
                self.change_rev_state(change['rev'], change['new'], change['old'])
                if(change['old']=='new'):
                    new2not+=1
                    if(change['new']=='ok'):
                        new2ok+=1
                    if(change['new']=='deferred'):
                        new2deferred+=1
            else:
                raise CodeReviewStatsException
        self.states.add('newly new')
        self.states.add('newly reviewed')
        self.states.add('newly ok')
        self.states.add('newly deferred')


class OutputSink(object):
    '''
    Object which keeps track of what/where to output stuff
    '''

    def __init__(self, format='javascript', snapshotfile=None):
        self.format = format
        self.snapshotfile = snapshotfile

    def output(self, tracker):
        if self.format == 'javascript':
            # print out ready-to-use javascript
            print "var crdata = " + json.dumps(tracker.get_snapshots_by_status(), sort_keys=True, indent=4) + ";"
        elif self.format == 'csv':
            self.output_csv(tracker)
        if self.snapshotfile is not None:
            self.write_final_snapshot(tracker)

    def output_csv(self, tracker):
        import csv
        csv_writer = csv.writer(sys.stdout)
        csv_writer.writerow(["date"] + list(tracker.states))

        for datekey in sorted(tracker.snapshots.keys()):
            timeobj = datetime.date.fromtimestamp(datekey/1000)
            row = [datetime.date.isoformat(timeobj)]
            for state in tracker.states:
                try:
                    row.append(tracker.snapshots[datekey][state])
                except KeyError:
                    row.append(0)
            csv_writer.writerow(row)

    def write_final_snapshot(self, tracker):
        fp = open(self.snapshotfile, 'w')
        json.dump(tracker.revisions, fp, sort_keys=True, indent=4)
        fp.close()


if __name__ == "__main__":
    '''Generate code review stats'''
    usage = "usage: %prog [options]"
    parser = optparse.OptionParser(usage)
    parser.add_option("-b", "--begindate", dest="begindate", type="str", default="2009-06-01")
    parser.add_option("-c", "--cache", dest="cache", type="str")
    parser.add_option("-d", "--dumpcache", dest="dumpcache", type="str", 
        help="Write the cache out as a CSV file for debug purposes")
    parser.add_option("-w", "--writecache", dest="writecache", type="str")
    parser.add_option("-s", "--snapshot", dest="snapshot", type="str")
    parser.add_option("-f", "--format", dest="format", type="str", default="javascript")
    parser.add_option("-q", "--querytag", dest="querytag", type="str", default="trunkphase3")
    (options, args) = parser.parse_args()

    # Perform db queries (or load from cache), and stuff results into a CodeReviewDB obj
    crdb = codereviewdb.CodeReviewDB(readcache=options.cache, writecache=options.writecache, dumpcache=options.dumpcache, querytag=options.querytag)
    # Process the results, taking periodic (e.g. daily) snapshots
    tracker = RevisionStateTracker(crdb, options.begindate)
    crdb.close()
    # Output the results in the selected format
    sink = OutputSink(snapshotfile=options.snapshot,format=options.format)
    sink.output(tracker)

