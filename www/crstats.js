
var orderedKeys = ['deferred', 'fixme', 'new', 'ok', 'old', 'resolved', 
	'reverted', 'verified', 'new (goal)', 'fixme (goal)',
	'newly new', 'newly reviewed', 'newly ok', 'newly deferred'];



function initChoiceContainer(data, divref) {
	// insert checkboxes
	var choiceContainer = $(divref);
	var checkedStr = '';
	var wanted = window.location.hash.substr(1).split(',');
	if( wanted.length <= 0 || ( wanted.length == 1 && wanted[0] == "" )) {
		wanted = [ 'fixme', 'new', 'new (goal)', 'fixme (goal)' ];
	}

	$.each(orderedKeys, function(index, key) {
		if( $.inArray( key, wanted ) != -1 ) {
			checkedStr = ' checked="checked"';
		}
		else {
			checkedStr = '';
		}
		choiceContainer.append('<br/><input type="checkbox" name="' + key +
			'"' + checkedStr + ' id="id' + key + '">' +
			'<label for="id' + key + '">' + '<a href="' + querylink + key + '">' 
			+ key + '</a></label>');
	});
	return choiceContainer;
}

$(document).ready(function(){
	var choiceContainer = initChoiceContainer(crdata, "#crstatecontrols");
	var chartdata=[];
	var states=[];
	var stateindex={};
	{
		var i=0;
		$.each(orderedKeys, function(index, key) {
			if(key in crdata) {
				val=crdata[key];
			}
			else {
				val=[];
			}
			chartdata.push(val);
			states.push(key);
			stateindex[key]=i;
			i++;
		});
	}
	var colorArray = ["#3465a4", //deferred
					  "#cc0000", //fixme
					  "#75507b", //new
					  "#73d216", //ok
					  "#babdb6", //old
					  "#ce5c00", //resolved
					  "#8f5902", //reverted
					  "#c4a000", //verified
					  "#a883ae", //new (goal)
					  "#ff3333", //fixme (goal)
					  "#a883ae", //newly new
					  "#555753", //newly reviewed
                      "#a6ee60", //newly ok
                      "#6d99d1"];//newly deferred 
    var newlyMarkerOptions = {
			show: true,
			style: 'diamond',
			lineWidth: 2,
			size: 6
		}
	var seriesOptions = [];
	seriesOptions[8] = {
		linePattern: 'dotted',
		shadow: false
	};
	seriesOptions[9] = {
		linePattern: 'dotted',
		shadow: false
	};
	seriesOptions[10] = {
		markerOptions: newlyMarkerOptions
	};
	seriesOptions[11] = {
		markerOptions: newlyMarkerOptions
	};
	seriesOptions[12] = {
		markerOptions: newlyMarkerOptions
	};
	seriesOptions[13] = {
		markerOptions: newlyMarkerOptions
	};
	var plot1 = $.jqplot('chart1', chartdata, {
		seriesColors: colorArray,
		axes:{
			xaxis:{
				renderer:$.jqplot.DateAxisRenderer, 
				tickOptions:{formatString:'%F'}
			}, 
			yaxis:{
				padMin:0,
				min:0,
				forceTickAt0:true,
				tickOptions:{
					formatString:"%d"
				}
			}
		},
		cursor:{
			show:true, 
			zoom:true, 
			constrainOutsideZoom: true, 
			showTooltip: false, 
			followMouse: true
		},
		highlighter:{
			show:true, 
			fadeTooltip: false
		},
		seriesDefaults: {
			showMarker: true,
			markerOptions: {
				show: false,
				style: 'filledCircle',
				lineWidth: 2,
				size: 9
			}
		},
		legend:{
			renderer: $.jqplot.EnhancedLegendRenderer,
			show:true,
			labels:states,
			rendererOptions:{
				numberRows:1
			},
			placement:'outside',
			location:'s',
			marginTop: '30px'
		},
		series:seriesOptions
	});
	$('.button-reset').click(function() { plot1.resetZoom(); plot1.replot({resetAxes:["yaxis"]}); });

	$("#crstatecontrols input").each(function (index) {
		plot1.series[stateindex[$(this).attr('name')]].show = $(this).is(':checked');
	});

	$("#crstatecontrols input").change(function () {
		plot1.series[stateindex[$(this).attr('name')]].show = $(this).is(':checked');
		plot1.axes.yaxis.resetDataBounds();
		plot1.replot({resetAxes:["yaxis"]});
	});
	plot1.replot({resetAxes:["yaxis"]});
	$(window).resize(function() {
		plot1.replot({resetAxes:true});
	});
});

