<?php
$reports = array();
$reports["trunkall"]=array(
	"data" => "data/trunkall/crstatsdata.js",
	"projection" => "projection.js",
	"title" => "All trunk revisions",
	"description" => "This graph is a history of nearly all commits to /trunk after r47450 and history since June 1, 2009. (Prior commits were subject to mass operations that obfuscated the data). Not included: checkins that span /trunk and some other directory.",
	"querylink" => "http://www.mediawiki.org/w/index.php?path=%2Ftrunk&title=Special%3ACode%2FMediaWiki%2Fstatus%2F"
	);
$reports["trunkphase3"]=array(
	"data" => "data/trunkphase3/crstatsdata.js",
	"title" => "All trunk/phase3 revisions",
	"description" => "This graph is a history of nearly all commits to /trunk/phase3 after r47450 and history since June 1, 2009. (Prior commits were subject to mass operations that obfuscated the data). Not included: checkins that span /trunk/phase3 and some other directory.",
	"querylink" => "http://www.mediawiki.org/w/index.php?path=%2Ftrunk%2Fphase3&title=Special%3ACode%2FMediaWiki%2Fstatus%2F"
	);
$reports["1.18all"]=array(
	"data" => "data/1.18all/crstatsdata.js",
	"title" => "1.18 all revisions",
	"description" => "This graph is a history of commits and state changes to /trunk between r47450 and r92475 since June 1, 2009 and before July 18, 2011. (Prior commits and state changes were subject to mass operations that obfuscated the data). Not included: checkins that span /trunk and some other directory.",
	"querylink" => "http://www.mediawiki.org/w/index.php?path=%2Ftrunk&offset=92475&title=Special%3ACode%2FMediaWiki%2Fstatus%2F"
	);
$reports["1.18phase3"]=array(
	"data" => "data/1.18phase3/crstatsdata.js",
	"title" => "1.18/phase3 revisions",
	"description" => "This graph is a history of commits and state changes to /trunk/phase3 between r47450 and r92475 since June 1, 2009 and before July 18, 2011. (Prior commits and state changes were subject to mass operations that obfuscated the data). Not included: checkins that span /trunk/phase3 and some other directory.",
	"querylink" => "http://www.mediawiki.org/w/index.php?path=%2Ftrunk%2Fphase3&offset=92475&title=Special%3ACode%2FMediaWiki%2Fstatus%2F"
	);


if( isset( $_GET["report"] ) && isset( $reports[$_GET["report"]] ) ) {
	$rep=$_GET["report"];
}
else {
	$rep="trunkall";
}

$toplinks = array();
foreach ($reports as $key => $rparams) {
	if($key==$rep) {
		$toplinks[] = '<b>'.$rparams['title'].'</b>';
	} else {	
		$toplinks[] = '<a href="?report='.$key.'">'.$rparams['title'].'</a>';
	}
}
$toplinkshtml = join(' - ', $toplinks);
?><!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="jqplot/jquery.jqplot.css" />
  <link rel="stylesheet" type="text/css" href="crstats.css" />
  
  <!--[if lt IE 9]><script language="javascript" type="text/javascript" src="jqplot/excanvas.js"></script><![endif]-->
  <!-- BEGIN: load jquery -->
  <script language="javascript" type="text/javascript" src="jqplot/jquery.js"></script>
  <!-- END: load jquery -->
  
  <!-- BEGIN: load jqplot -->
  <script language="javascript" type="text/javascript" src="jqplot/jquery.jqplot.js"></script>
  <script language="javascript" type="text/javascript" src="jqplot/plugins/jqplot.cursor.js"></script>
  <script language="javascript" type="text/javascript" src="jqplot/plugins/jqplot.dateAxisRenderer.js"></script>
  <script language="javascript" type="text/javascript" src="jqplot/plugins/jqplot.highlighter.js"></script>
  <script language="javascript" type="text/javascript" src="jqplot/plugins/jqplot.enhancedLegendRenderer.js"></script>
  <script language="javascript" type="text/javascript">var querylink='<?php print $reports[$rep]["querylink"]; ?>';</script>
  <script id="data" language="javascript" type="text/javascript" src="<?php print $reports[$rep]["data"]; ?>"></script> 
<?php
  if ( isset( $reports[$rep]["projection"] ) ) {
	  echo "<script id='projection' language='javascript' type='text/javascript' src='{$reports[$rep]['projection']}'></script>\n";
  }
?>
  <script type="text/javascript" language="javascript" src="crstats.js"></script>
		
</head>
<body>
<h1 id="titleline">Code Review Statistics</h1>
<div id="crchartselect"><?php print $toplinkshtml; ?></div>
<div>
	<div id="chart1out">
		<div class="jqPlot" id="chart1"></div>
	</div>
	<div id="crcontrols">
		<div id="crstatecontrols"></div>
		<div id="crcontrolbuttons"><button class="button-reset">Reset Zoom</button></div> 
	</div>
</div>
<div id="crbottomtext">
<p><?php print $reports[$rep]["description"]; ?></p>
<p>The "newly new" psuedo-state is a plot of new revisions added in a given week.  The "reviewed" psuedo-state is all revisions that transition from "new" to some other state in a given week.</p>
</div>
<p>(thanks to Bryan Tong Minh for his <a href="http://toolserver.org/~bryan/stats/codereview-status-diff.png">original version</a>)</p>

<p>Powered by <a href="http://toolserver.org">Wikimedia Toolserver</a>.</p>
</body>


</html>
